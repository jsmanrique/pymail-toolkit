Simple Python mailing toolkit
=============================

This is just a very simple Python script to send emails to a all the people listed in a .csv file
using a SMTP server configured in .cfg file.

Usage
-----

You would need the following files
* A configuration file, with info about the SMTP server (address, port, username, domain). You can edit the
one provided (`pymail.cfg`), that it would be use by default, or create your own one and pass it as an
argument.
* A .csv file with a set of rows containing name,email of the people you want to write to. You can see and example
in `emails.csv`.
* A file with the message to be sent. You can use `${person_name}` as placeholder for people name. You can see
an example in `exampl.tmpl`.

To send the emails, just go to the command line:

```
pymail.py [-c SMTP_CONFIG_FILE] -s SUBJECT -m MESSAGE_TMPL_FILE -r RECIPIENTS_FILE
```

Requirements
------------

I think there are no major requirements, since almost every module used might be installed 
with your Python environment:

* smtplib
* email.mime.multipart, email.mime.text
* argparse, configparser
* getpass
* string
* csv

License
--------

This script is mostly based on https://medium.freecodecamp.com/send-emails-using-code-4fcea9df63f

In any case, I distribute this one under MIT License
