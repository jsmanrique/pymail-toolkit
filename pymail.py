#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Python script to send templated emails to a distribution lists
# Mostly based on https://medium.freecodecamp.com/send-emails-using-code-4fcea9df63f
#
# Copyright (C) 2017 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import argparse
import configparser

import getpass

from string import Template

import csv

import logging
logging.basicConfig(level=logging.INFO)

def parse_args(args):

    parser = argparse.ArgumentParser(description = 'Send an email to a each person defined in a .csv file')
    parser.add_argument('-s', '--settings', dest = 'smtp_config_file', default='pymail.cfg', help = 'SMTP server settings')
    parser.add_argument('-t', '--subject', dest = 'subject', required = True, help = 'Email subject string')
    parser.add_argument('-m', '--message', dest = 'message_tmpl', required = True, help = 'Email message template file')
    parser.add_argument('-r', '--recipients', dest = 'recipients_file', required = True, help = 'Contacts .csv file')
    parser.add_argument('-c', '--carboncopied', dest = 'cc_recipients', default='', help = 'emails addresses to send CC-ed the email')

    return parser.parse_args()

def get_contacts_data(filename):
    """
    Returns a list of [name, email, <optional 3rd parameter>] 
    read from a .csv file specified by filename.
    """

    contacts_data = []
    with open(filename, mode='r', encoding='utf-8') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            row_data = [row[0].split(' ')[0], row[1]]
            try:
                row_data.append(row[2])
            except IndexError as e:
                logging.info("Third parameter was not set in the .csv file")
                pass
            contacts_data.append(row_data)

    logging.info("{} people info parsed".format(len(contacts_data)))

    return contacts_data

def read_template(filename):
    """
    Returns a Template object comprising the contents of the
    file specified by filename.
    """

    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()

    logging.info("{} loaded as template file".format(filename))

    return Template(template_file_content)

def parse_smtp_config_file(filename):
    """
    Returns settings (address, port, username, email domain) to connect to
    the SMTP server
    """
    smtp_config = configparser.ConfigParser()
    smtp_config.read(filename)
    smtp_server = smtp_config.get('SMTP_Settings', 'smtp_server')
    smtp_port = smtp_config.getint('SMTP_Settings', 'smtp_port')
    smtp_user = smtp_config.get('SMTP_Settings', 'smtp_user')
    smtp_domain = smtp_config.get('SMTP_Settings', 'smtp_domain')

    return smtp_server, smtp_port, smtp_user, smtp_domain


def main(args):
    args = parse_args(args)

    contacts_data = get_contacts_data(args.recipients_file)

    message_template = read_template(args.message_tmpl)

    server, port, user, domain = parse_smtp_config_file(args.smtp_config_file)

    server_connection = smtplib.SMTP(server, port)
    server_connection.starttls()
    logging.info("Connecting to {}:{}".format(server, port))
    smtp_pass = getpass.getpass(prompt='SMTP server password for '+ user +': ')
    server_connection.login(user, smtp_pass)

    emails_count = 0

    for contact_data in contacts_data:

        msg = MIMEMultipart()

        if len(contact_data) == 3:
            message = message_template.substitute(
                person_name = contact_data[0],
                extra = contact_data[2]
            )
        else:
            try:
                message = message_template.substitute(
                    person_name = contact_data[0]
                )
            except KeyError as e:
                logging.info('Error in the template. Template values not provided: {}'.format(e))
                quit()

        # print(message)
        msg['From'] = user+'@'+domain
        msg['To'] = contact_data[1]
        if args.cc_recipients :
            msg['cc'] = args.cc_recipients
        msg['Subject']=args.subject

        msg.attach(MIMEText(message, 'plain'))

        server_connection.send_message(msg)

        emails_count = emails_count + 1

        logging.info("Message count: {}. Message sent to {} <{}>".format(emails_count, contact_data[0], contact_data[1]))

        del msg

    server_connection.quit()

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
